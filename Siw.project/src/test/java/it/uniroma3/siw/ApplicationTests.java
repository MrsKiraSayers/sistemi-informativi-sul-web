package it.uniroma3.siw;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import it.uniroma3.siw.service.*;
import junit.framework.*;
import it.uniroma3.siw.repository.*;
import it.uniroma3.siw.model.*;
import java.util.List;



@SpringBootTest
@RunWith(SpringRunner.class)
class ApplicationTests {

	@Autowired
	private UserService userService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private ProjectService ProjectService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TaskRepository taskRepository;
	@Autowired
	private ProjectRepository projectRepository;
	
	@Before
	public void deleteAll() {
		System.out.println("deleting all data in DB...");
		userRepository.deleteAll();
		taskRepository.deleteAll();
		projectRepository.deleteAll();
		System.out.println("done");
	}
	
	
	
	@SuppressWarnings("deprecation")	
	@Test
	public void testUpdateUser() {
		//save first user in db
		User user1 = new User("mariorossi","password","mario","rossi");
		user1 = userService.saveUser(user1);
		Assert.assertEquals(user1.getId().longValue(), 1L);
		Assert.assertEquals(user1.getUserName(), "mariorossi");
		Assert.assertEquals(user1.getFirstName(), "Mario");
		Assert.assertEquals(user1.getLastName(), "Rossi");
		
		User user1updated = new User("mariarossi","password","Maria","Rossi");
		user1updated.setId(user1.getId());
		user1updated = userService.saveUser(user1updated);
		user1updated = userService.getUser(user1updated.getId());
		Assert.assertEquals(user1updated.getId().longValue(), 1L);
		Assert.assertEquals(user1updated.getUserName(), "mariarossi");
		Assert.assertEquals(user1updated.getFirstName(), "Maria");
		Assert.assertEquals(user1updated.getLastName(), "Rossi");
		
		User user2 = new User("lucabianchi","password","Luca","Bianchi");
		user2 = userService.saveUser(user2);
		Assert.assertEquals(user2.getId().longValue(), 2L);
		Assert.assertEquals(user2.getUserName(), "lucabianchi");
		Assert.assertEquals(user2.getFirstName(), "Luca");
		Assert.assertEquals(user2.getLastName(), "Bianchi");
		
		Project project1 = new Project("TestProject","just a little");
		project1.setOwner(user1);
		project1=ProjectService.saveProject(project1);
		Assert.assertEquals(project1.getOwner(), user1);
		Assert.assertEquals(project1.getName(),"TestProject");
		Assert.assertEquals(project1.getDescription(),"just a little");
		
		Project project2 = new Project("TestProject1","just a little1");
		project2.setOwner(user1);
		project2=ProjectService.saveProject(project2);
		Assert.assertEquals(project2.getOwner(), user1);
		Assert.assertEquals(project2.getName(),"TestProject");
		Assert.assertEquals(project2.getDescription(),"just a little");
		
		project1 = ProjectService.shareProjectWithUser(project1, user2);
		
		List<Project> projects = projectRepository.findByOwner(user1updated);
		Assert.assertEquals(projects.size(), 2);
		Assert.assertEquals(projects.get(0), project1);
		Assert.assertEquals(projects.get(1), project2);
		
		/*List<User> project1members = UserRepository.findByVisibleProjects(project1);
		Assert.assertEquals(project1members.size(), 1);
		Assert.assertEquals(project1members.get(0), user2);
		
		List<Project> projectsVisibleByUser2 = ProjectRepository.findByMembers(user2);
		Assert.assertEquals(projectsVisibleByUser2.size(), 1);
		Assert.assertEquals(projectsVisibleByUser2.get(0), project1);*/
		
	}

}
