package it.uniroma3.siw.repository;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.model.*;

@Repository
public interface CredentialsRepository extends CrudRepository<Credentials, Long>{
	
	public Optional<Credentials> findByUsername(String username);
}
